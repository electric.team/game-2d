# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Projekty/game-2d/include/Entity.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/Include/Entity.cpp.obj"
  "C:/Projekty/game-2d/include/Game.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/Include/Game.cpp.obj"
  "C:/Projekty/game-2d/include/settings.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/Include/settings.cpp.obj"
  "C:/Projekty/game-2d/main.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/main.cpp.obj"
  "C:/Projekty/game-2d/src/BasicButton.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/BasicButton.cpp.obj"
  "C:/Projekty/game-2d/src/Block.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/Block.cpp.obj"
  "C:/Projekty/game-2d/src/Button.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/Button.cpp.obj"
  "C:/Projekty/game-2d/src/GameState.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/GameState.cpp.obj"
  "C:/Projekty/game-2d/src/MenuState.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/MenuState.cpp.obj"
  "C:/Projekty/game-2d/src/PauseState.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/PauseState.cpp.obj"
  "C:/Projekty/game-2d/src/PlayState.cpp" "C:/Projekty/game-2d/cmake-build-debug/CMakeFiles/SFML_GAME.dir/src/PlayState.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "SFML_STATIC"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "C:/SFML-2.4.2/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
